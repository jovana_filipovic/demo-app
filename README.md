👩🏼‍💻

# Setup

Run npm install to install dependencies.

## Start

Run npm start.
Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
