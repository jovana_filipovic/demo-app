import { v4 } from "uuid";
import { List } from "models/list";

const initialListItems: List = [
  {
    id: v4(),
    title: "List Item 1",
    completed: false,
  },
  {
    id: v4(),
    title: "List Item 2",
    completed: false,
  },
  {
    id: v4(),
    title: "List Item 3",
    completed: false,
  },
];

export default initialListItems;
