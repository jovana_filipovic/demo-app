import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { ListItemProvider, ThemeProvider } from "components";

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider>
      <ListItemProvider>
        <App />
      </ListItemProvider>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
