import { createContext, useContext, useState } from "react";
import { ThemeProviderProps, ThemeContextProps } from "./ThemeContext.types";

const initialState: ThemeContextProps = {
  isDark: false,
};

export const ThemeContext = createContext<ThemeContextProps>(initialState);

export const useThemeContext = () => useContext(ThemeContext);

export default function ThemeProvider({ children }: ThemeProviderProps) {
  const [isDark, setIsDark] = useState(initialState.isDark);

  const toggleTheme = () => {
    setIsDark(!isDark);
  };
  return (
    <ThemeContext.Provider value={{ isDark, toggleTheme }}>
      {children}
    </ThemeContext.Provider>
  );
}
