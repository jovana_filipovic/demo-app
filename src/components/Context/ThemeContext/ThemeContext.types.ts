export type ThemeContextProps = {
  isDark: boolean;
  toggleTheme?: () => void;
};

export type ThemeProviderProps = {
  children?: React.ReactNode;
};
