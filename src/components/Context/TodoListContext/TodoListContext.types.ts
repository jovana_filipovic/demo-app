import { List } from "models/list";
import { ListItem } from "models/listItem";

export type TodoListContextProps = {
  listItems: List;
  addListItem?: (listItem: Partial<ListItem>) => void;
  editListItem?: (id: string, title: string) => void;
  deleteListItem?: (id: string) => void;
  setListItemStatus?: (id: string, completed: boolean) => void;
};

export type ListItemProviderProps = {
  children?: React.ReactNode;
};
