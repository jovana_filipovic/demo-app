import { createContext, useContext, useState } from "react";
import { v4 } from "uuid";
import initialListItems from "mocks";
import { ListItem } from "models/listItem";
import {
  ListItemProviderProps,
  TodoListContextProps,
} from "./TodoListContext.types";

const initialState: TodoListContextProps = {
  listItems: initialListItems,
};

export const TodoListContext =
  createContext<TodoListContextProps>(initialState);

export const useTodoListContext = () => useContext(TodoListContext);

export default function ListItemProvider({ children }: ListItemProviderProps) {
  const [listItems, setListItems] = useState(initialState.listItems);

  const addListItem = (listItem: Partial<ListItem>) => {
    setListItems([...listItems, { id: v4(), ...listItem }]);
  };

  const editListItem = (id: string, title: string) => {
    setListItems(
      listItems.map((item) => (item.id === id ? { ...item, title } : item))
    );
  };

  const deleteListItem = (id: string) => {
    setListItems(listItems.filter((item) => item.id !== id));
  };

  const setListItemStatus = (id: string, completed: boolean) => {
    setListItems(
      listItems.map((item) => (item.id === id ? { ...item, completed } : item))
    );
  };

  return (
    <TodoListContext.Provider
      value={{
        listItems,
        addListItem,
        editListItem,
        deleteListItem,
        setListItemStatus,
      }}
    >
      {children}
    </TodoListContext.Provider>
  );
}
