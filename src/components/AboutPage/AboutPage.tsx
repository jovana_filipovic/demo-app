import { TodoListContext, ThemeContext } from "components/Context";
import labels from "./AboutPage.labels";
import classNames from "classnames";
import "./AboutPage.styles.scss";
import { List } from "models/list";

/**
 * Example using Context.Consumer instead od useContext hook
 */
const AboutPage = () => {
  const totalItems = (listItems: List) => (
    <h3>
      {labels.total}
      {listItems.length}
    </h3>
  );

  const completedItems = (listItems: List) => (
    <h3>
      {labels.completed}
      {listItems.filter((item) => item.completed).length}
    </h3>
  );

  return (
    <ThemeContext.Consumer>
      {({ isDark }) => (
        <TodoListContext.Consumer>
          {({ listItems }) => (
            <div
              className={classNames("about-page", {
                "about-page--dark": isDark,
              })}
            >
              <h1>{labels.title}</h1>
              {totalItems(listItems)}
              {completedItems(listItems)}
            </div>
          )}
        </TodoListContext.Consumer>
      )}
    </ThemeContext.Consumer>
  );
};
export default AboutPage;
