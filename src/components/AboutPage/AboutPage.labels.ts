const labels = {
  title: "About",
  total: "Total number of items: ",
  completed: "✅ Completed items: ",
};

export default labels;
