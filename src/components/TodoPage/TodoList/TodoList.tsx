import "./TodoList.styles.scss";
import TodoListItem from "../TodoListItem/TodoListItem";
import { useTodoListContext } from "components/Context";
import labels from "./TodoList.labels";

const TodoList = () => {
  const { listItems } = useTodoListContext();

  return (
    <>
      <div className="todo-list">
        {listItems.length === 0 && <h3>{labels.emptyList}</h3>}
        {listItems.map((item) => (
          <TodoListItem key={item.id} {...item} />
        ))}
      </div>
    </>
  );
};
export default TodoList;
