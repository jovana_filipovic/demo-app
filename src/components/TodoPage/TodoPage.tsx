import classNames from "classnames";
import { useThemeContext } from "components/Context";
import AddNewItem from "./AddNewItem/AddNewItem";
import TodoList from "./TodoList/TodoList";
import labels from "./TodoPage.labels";
import "./TodoPage.styles.scss";

const TodoPage = () => {
  const { isDark } = useThemeContext();
  return (
    <div className={classNames("todo-page", { "todo-page--dark": isDark })}>
      <h1>{labels.title}</h1>
      <AddNewItem />
      <TodoList />
    </div>
  );
};
export default TodoPage;
