const labels = {
  placeholder: "Add new item ...",
  buttonLabel: "Add",
};
export default labels;
