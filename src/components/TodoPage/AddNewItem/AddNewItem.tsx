import { ChangeEvent, useState } from "react";
import classNames from "classnames";
import { useTodoListContext, useThemeContext } from "components/Context";
import labels from "./AddNewItem.labels";
import "./AddNewItem.styles.scss";

const AddNewItem = () => {
  const [newItemTitle, setNewItemTitle] = useState<string>("");
  const { addListItem } = useTodoListContext();
  const { isDark } = useThemeContext();

  return (
    <div
      className={classNames("add-new-item-container", {
        "add-new-item-container--dark": isDark,
      })}
    >
      <input
        type="text"
        placeholder={labels.placeholder}
        value={newItemTitle}
        onChange={(e: ChangeEvent<HTMLInputElement>) => {
          setNewItemTitle(e.target.value);
        }}
      />
      <button
        onClick={() => {
          if (newItemTitle === "") return;
          addListItem?.({ title: newItemTitle, completed: false });
          setNewItemTitle("");
        }}
      >
        {labels.buttonLabel}
      </button>
    </div>
  );
};
export default AddNewItem;
