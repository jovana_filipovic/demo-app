export type TodoListItemProps = {
  id: string;
  title?: string;
  completed?: boolean;
};
