import { FocusEvent, ChangeEvent, useState } from "react";
import classNames from "classnames";
import { useTodoListContext, useThemeContext } from "components/Context";
import { TodoListItemProps } from "./TodoListItem.types";
import "./TodoListItem.styles.scss";

const TodoListItem = ({ id, title, completed }: TodoListItemProps) => {
  const { setListItemStatus, deleteListItem, editListItem } =
    useTodoListContext();
  const { isDark } = useThemeContext();
  const [isEdit, setIsEdit] = useState(false);

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setListItemStatus?.(id, event.target.checked);
  };

  const handleClick = (id: string) => {
    if (!id) return;
    deleteListItem?.(id);
  };

  const handleOnBLur = (event: FocusEvent<HTMLInputElement>) => {
    setIsEdit(false);
    if (event.target.value !== "") editListItem?.(id, event.target.value);
  };

  return (
    <div
      className={classNames("todo-list-item", {
        "todo-list-item--completed": completed,
        "todo-list-item--dark": isDark,
      })}
    >
      <div className="todo-list-item__input">
        <input
          type="checkbox"
          checked={completed ?? false}
          onChange={handleChange}
        />
        {isEdit ? (
          <input
            type="text"
            defaultValue={title}
            onBlur={(e: FocusEvent<HTMLInputElement>) => handleOnBLur(e)}
          />
        ) : (
          <span>{title}</span>
        )}
      </div>
      <div className="todo-list-item__buttons">
        <i className="fa fa-pencil" onClick={() => setIsEdit(!isEdit)} />
        <i className="fa fa-trash" onClick={() => handleClick(id)} />
      </div>
    </div>
  );
};
export default TodoListItem;
