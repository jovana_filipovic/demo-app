const labels = {
  about: "About",
  todo: "Todo List",
  lightTheme: "🌞 Theme",
  darkTheme: "🌚 Theme",
};
export default labels;
