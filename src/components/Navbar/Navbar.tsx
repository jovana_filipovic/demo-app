import { Link } from "react-router-dom";
import classNames from "classnames";
import { useThemeContext } from "components/Context";
import labels from "./Navbar.labels";
import "./Navbar.styles.scss";

const Navbar = () => {
  const { isDark, toggleTheme } = useThemeContext();

  return (
    <div
      className={classNames("nav-bar", {
        "nav-bar--dark": isDark,
      })}
    >
      <Link to="/">{labels.about}</Link>
      <Link to="/todos">{labels.todo}</Link>
      <button onClick={toggleTheme}>
        {isDark ? labels.lightTheme : labels.darkTheme}
      </button>
    </div>
  );
};

export default Navbar;
