export type ListItem = {
  id: string;
  title?: string;
  completed?: boolean;
};
