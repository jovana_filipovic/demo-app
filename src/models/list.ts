import { ListItem } from "./listItem";

export type List = Array<ListItem>;
