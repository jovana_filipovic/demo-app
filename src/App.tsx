import { BrowserRouter as Router, Route } from "react-router-dom";
import classNames from "classnames";
import { Navbar, TodoPage, AboutPage } from "components";
import { useThemeContext } from "components/Context";
import "./App.scss";

function App() {
  const { isDark } = useThemeContext();

  return (
    <Router>
      <div className={classNames("app", { "app--dark": isDark })}>
        <Navbar />
        <Route path="/" exact component={AboutPage} />
        <Route path="/todos" exact component={TodoPage} />
      </div>
    </Router>
  );
}

export default App;
